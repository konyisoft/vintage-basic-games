import { Program } from '../programs';

export const SINE_WAVE: Program =
{
  title: 'Sine wave',
  id: 'sine-wave',
  description: 'Draw a sine wave on screen',
  code:
  `
  10 PRINT "SINE WAVE"
  20 PRINT "CREATIVE COMPUTING  MORRISTOWN, NEW JERSEY"
  30 PRINT: PRINT: PRINT: PRINT: PRINT
  40 REMARKABLE PROGRAM BY DAVID AHL
  50 B=0
  100 REM  START LONG LOOP
  110 FOR T=0 TO 40 STEP .25
  120 A=INT(26+25*SIN(T))
  130 PRINT TAB(A);
  140 IF B=1 THEN 180
  150 PRINT "CREATIVE"
  160 B=1
  170 GOTO 200
  180 PRINT "COMPUTING"
  190 B=0
  200 NEXT T
  999 END
  `
}
