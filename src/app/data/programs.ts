import * as Prg from './sources';

export interface Program {
  id: string;
	title: string;
	description: string;
  options?: any;
	code: string;
}

export interface Category {
  title: string;
  id: string;
  icon: string;
  programs: Program[];
}

export interface ProgramData {
  categories: Category[];
}

export const PROGRAM_DATA: ProgramData =
{
  categories: [
    {
      title: 'Adventure',
      id: 'adventure',
      icon: 'compass',
      programs: [
        Prg.CIA_ADVENTURE,
        Prg.TOWER_ADVENTURE,
        //Prg.WEREWOLVES_AND_WANDERER,
        Prg.WUMPUS_1
        //Prg.WUMPUS_2
      ]
    },
    {
      title: 'Card and board',
      id: 'card-and-board',
      icon: 'heart',
      programs: [
        Prg.ACEY_DUCEY,
        Prg.BLACK_JACK,
        Prg.CHECKERS,
        Prg.GOMOKO,
        Prg.WAR
      ]
    },
    {
      title: 'Combat',
      id: 'combat',
      icon: 'locate',
      programs: [
        Prg.BOMBS_AWAY,
        Prg.COMBAT,
        Prg.GUNNER
      ]
    },
    {
      title: 'Educational',
      id: 'educational',
      icon: 'school',
      programs: [
        Prg.ANIMAL,
        Prg.CAMEL,
        Prg.CHANGE,
        Prg.CHEMIST,
        Prg.CHIEF,
        Prg.CIVIL_WAR,
        Prg.FUR_TRADER,
        Prg.HAMURABI,
        Prg.HANGMAN,
        Prg.KINEMA,
        Prg.KING,
        Prg.MATH_DICE,
        Prg.STOCK_MARKET,
        Prg.SYNONYM,
        Prg.TRAIN
      ]
    },
    {
      title: 'Gambling and casino',
      id: 'gambling-and-casino',
      icon: 'cash',
      programs: [
        Prg.CRAPS,
        Prg.DICE,
        Prg.HORSERACE,
        Prg.POKER,
        Prg.ROULETTE,
        Prg.SLOTS
      ]
    },
    {
      title: 'Introductory fun',
      id: 'introductory-fun',
      icon: 'wine',
      programs: [
        Prg.BUZZWORD,
        Prg.HELLO,
        Prg.NAME,
        Prg.POETRY,
        Prg.ROCK_SCISSORS_PAPER,
        Prg.RUSSIAN_ROULETTE,
        Prg.WEEKDAY
      ]
    },
    {
      title: 'Logic',
      id: 'logic',
      icon: 'infinite',
      programs: [
        Prg.AWARI,
        Prg.BAGELS,
        Prg.CHOMP,
        Prg.CUBE,
        Prg.FLIPFLOP,
        Prg.HEXAPAWN,
        Prg.HIGH_IQ,
        Prg.MASTERMIND,
        Prg.NICOMACHUS,
        Prg.ONE_CHECK,
        Prg.QUBIC,
        Prg.QUEEN,
        Prg.REVERSE,
        Prg.TIC_TAC_TOE_1,
        Prg.TIC_TAC_TOE_2,
        Prg.TOWERS
      ]
    },
    {
      title: 'Matrix manipulation',
      id: 'matrix-manipulation',
      icon: 'grid',
      programs: [
        Prg.BATTLE,
        Prg.BOMBARDMENT,
        Prg.DEPTH_CHARGE,
        Prg.HURKLE,
        Prg.MUGWUMP,
        Prg.PIZZA,
        Prg.SALVO
      ]
    },
    {
      title: 'Miscellaneous',
      id: 'miscellaneous',
      icon: 'cube',
      programs: [
        Prg.ELIZA_1,
        Prg.ELIZA_2
      ]
    },
    {
      title: 'Number or letter guessing',
      id: 'numbner-or-letter-guessing',
      icon: 'help-circle',
      programs: [
        Prg.GUESS,
        Prg.HI_LO,
        Prg.LETTER,
        Prg.NUMBER,
        Prg.STARS,
        Prg.TRAP,
        Prg.WORD
      ]
    },
    {
      title: 'Plotting and pictures',
      id: 'plotting-and-picture',
      icon: 'images',
      programs: [
        Prg.AMAZING,
        Prg.BUG,
        Prg.BUNNY,
        Prg.LOVE,
        Prg.SINE_WAVE
      ]
    },
    {
      title: 'Remove an object',
      id: 'remove-an-object',
      icon: 'hand',
      programs: [
        Prg.MATCHES_23,
        Prg.BATNUM,
        Prg.EVEN_WINS,
        Prg.GAME_OF_EVEN_WINS,
        Prg.NIM
      ]
    },
    {
      title: 'Space',
      id: 'space',
      icon: 'rocket',
      programs: [
        Prg.LEM,
        Prg.LUNAR,
        Prg.ROCKET,
        Prg.ORBIT,
        Prg.SPLAT,
        Prg.SUPER_STAR_TREK,
        Prg.SUPER_STAR_TREK_INSTRUCTIONS,
        Prg.TARGET
      ]
    },
    {
      title: 'Sports simulation',
      id: 'sport',
      icon: 'american-football',
      programs: [
        Prg.BASKETBALL,
        Prg.BOWLING,
        Prg.BOXING,
        Prg.BULLFIGHT,
        Prg.BULLSEYE,
        Prg.FOOTBALL,
        Prg.FTBALL,
        Prg.GOLF,
        Prg.HOCKEY,
        Prg.SLALOM
      ]
    },
  ]
}
