import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';

import { ProgramService } from '../../services/program.service';
import { Program } from 'src/app/data/programs';
import { LegendService } from '../../services/legend.service';
import { LegendComponent } from '../../components/legend/legend.component';

@Component({
  selector: 'app-programs',
  templateUrl: './programs.page.html',
  styleUrls: ['./programs.page.scss'],
})
export class ProgramsPage implements OnInit {

  constructor(
    private router: Router,
    private popoverController: PopoverController,
    public programService: ProgramService,
    public legendService: LegendService
  ) { }

  ngOnInit() {
  }

  get HeaderText(): string {
    let count: number = this.programService.SelectedCategory.programs.length;
    if (count == 0) {
      return 'No games here :(';
    } else if (count == 1) {
      return 'Only 1 game here';
    } else {
      return count + ' games here';
    }
  }

  async onLegendClick() {
    const popover = await this.popoverController.create({
      component: LegendComponent,
      event: event,
      cssClass: 'popover-legend',
      showBackdrop: false,
    });
    this.legendService.setPopoverVisibility(true);

    // Fires after popover closed
    popover.onDidDismiss().then((data: any) => {
      this.legendService.setPopoverVisibility(false);
    });

    return await popover.present();
  }

  onProgramClick(index: number) {
    this.programService.selectProgram(this.programService.SelectedCategory, index);
    this.router.navigate(['/game']);
  }

  getIconName(program: Program): string {
    let name = 'list'; // default icon name
    if (program && program.options) {
      if (program.options.buggy) {
        name = 'bug';
      } else if (program.options.landscape) {
        name = 'phone-landscape';
      } else if (program.options.nohelp) {
        name = 'flashlight';
      }
    }
    return name;
  }

}
