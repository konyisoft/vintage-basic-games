import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProgramsPage } from './programs.page';
import { ComponentsModule } from '../../components/components.module';
import { LegendComponent } from '../../components/legend/legend.component';

const routes: Routes = [
  {
    path: '',
    component: ProgramsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    ProgramsPage
  ],
  entryComponents: [
    LegendComponent
  ]
})
export class ProgramsPageModule {}
