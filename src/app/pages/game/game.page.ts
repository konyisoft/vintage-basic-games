import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { ToastController, PopoverController } from '@ionic/angular';

import { BasicService } from '../../services/basic.service';
import { ProgramService } from '../../services/program.service';
import { SettingsService } from '../../services/settings.service';
import { Program } from '../../data/programs';
import { ChatComponent } from '../../components/chat/chat.component';
import { SettingsComponent } from '../../components/settings/settings.component';

@Component({
  selector: 'app-game',
  templateUrl: './game.page.html',
  styleUrls: ['./game.page.scss'],
})
export class GamePage implements OnInit, AfterViewInit {

  @ViewChild(ChatComponent, {static: false})
  private chat: ChatComponent;

  private title: string;
  private currentProgram: Program;

  constructor(
    private toastController: ToastController,
    private popoverController: PopoverController,
    public basicService: BasicService,
    public programService: ProgramService,
    public settingsService: SettingsService
  ) { }

  ngOnInit() {
    if (this.programService.HasSelectedProgram) {
      this.startProgram(this.programService.SelectedProgram);
    }
    // For development only:
    //this.startProgram(this.programService.Categories[1].programs[0]);
  }

  ngAfterViewInit() {
    if (this.currentProgram) {
      this.chat.scrollToBottom();
    }
  }

  onRestartClick() {
    if (this.currentProgram) {
      //console.log('Restart program');
      let toastMessage: string = this.basicService.IsRunning ? 'Program restarted' : 'Program started';
      this.basicService.restart();
      this.chat.reset();
      this.presentToast(toastMessage);
    }
  }

  onStopClick() {
    if (this.currentProgram && this.basicService.IsRunning) {
      //console.log('Stop program');
      this.basicService.stop();
      this.chat.reset();
      this.chat.scrollToBottom();
      this.presentToast('Program stopped');
    }
  }

  async onSettingsClick(event: any) {
    const popover = await this.popoverController.create({
      component: SettingsComponent,
      event: event,
      cssClass: 'popover-settings',
      showBackdrop: false,
    });
    this.settingsService.setPopoverVisibility(true);

    // Fires after popover closed
    popover.onDidDismiss().then((data: any) => {
      this.settingsService.save();
      this.settingsService.setPopoverVisibility(false);
    });

    return await popover.present();
  }

  get Title(): string {
    return this.title;
  }

  private startProgram(program: Program) {
    if (program) {
      this.title = program.title;
      this.basicService.open(program.code);
      this.currentProgram = program;
    }
  }

  private async presentToast(message) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
      position: 'middle',
      color: 'dark',
      cssClass: 'custom-toast'
    });
    toast.present();
  }

}
