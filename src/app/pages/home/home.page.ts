import { OnInit, Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { ProgramService } from '../../services/program.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  private backButtonSubscription: any;

  constructor(
    private router: Router,
    private platform: Platform,
    public programService: ProgramService
  ) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      navigator['app'].exitApp();
    });
}

  ionViewWillLeave() {
    this.backButtonSubscription.unsubscribe();
  }

  onSearchClick() {
    // TODO: implement it
  }

  onCategoryClick(index: number) {
    this.programService.selectCategory(index);
    this.router.navigate(['/programs']);
  }

  getCategoryClass(categoryIndex: number): string {
    let colorCount: number = 10;
    return 'category-' + (categoryIndex % colorCount);
  }
}
