import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings.service';
import { UtilsService } from '../../services/utils.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {

  constructor(
    public settingsService: SettingsService,
    public utils: UtilsService
  ) { }

  ngOnInit() { }

  onResetValues() {
    this.settingsService.reset();
  }

}
