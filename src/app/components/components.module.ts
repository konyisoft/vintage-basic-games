import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { ChatComponent } from './chat/chat.component';
import { SettingsComponent } from './settings/settings.component';
import { LegendComponent } from './legend/legend.component';

import { SafeHtmlPipe } from '../pipes/safehtml.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule.forRoot()
  ],
  declarations: [
    ChatComponent,
    SettingsComponent,
    LegendComponent,
    SafeHtmlPipe
  ],
  exports: [
    ChatComponent,
    SettingsComponent,
    LegendComponent
  ],
  providers: [
    SafeHtmlPipe
  ]
})
export class ComponentsModule { }
