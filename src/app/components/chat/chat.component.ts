import { Component, OnInit /*, ViewChild, ElementRef*/ } from '@angular/core';

import { BasicService, MessageType } from '../../services/basic.service';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  message: string = '';
  MessageType = MessageType;

  private timeoutId: any;

  private SCROLL_DELAY: number = 100; // in millisecs

  constructor(
    public basicService: BasicService,
    public settingsService: SettingsService
  ) { }

  ngOnInit() {
  }

  reset() {
    this.cancelTimeout();
    this.message = '';
  }

  onMessageKeyup(event: any) {
    // Enter key pressed
    if (event.keyCode == 13) {
      this.onSendMessageClick();
    }
  }

  onSendMessageClick() {
    if (this.basicService.CanReceiveInput) {
      this.basicService.input(this.message, true);
      this.message = '';  // clear input field
      this.scrollToBottom();
    }
  }

  getClasses(messageType: MessageType) {
    return {
      input: messageType == MessageType.Input,
      output: messageType == MessageType.Output,
      warning: messageType == MessageType.Warning,
      error: messageType == MessageType.Error
    };
  }

  scrollToBottom() {
    this.cancelTimeout();
    this.timeoutId = setTimeout(() => {
      this.doScrollToBottom();
    }, this.SCROLL_DELAY);
  }

  private doScrollToBottom() {
    $('#message-area').animate({
      scrollTop: $('#message-area')[0].scrollHeight
    }, "slow");
  }

  private cancelTimeout() {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
  }

}
