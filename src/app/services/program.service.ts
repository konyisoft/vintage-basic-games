import { Injectable } from '@angular/core';
import { Category, Program, PROGRAM_DATA } from '../data/programs';

@Injectable({
  providedIn: 'root'
})
export class ProgramService {

  private categories: Category[] = PROGRAM_DATA.categories;
  private selectedCategory: Category;
  private selectedProgram: Program;
  private programCount: number;

  constructor() {
    this.programCount = this.getProgramCount();
  }

  selectCategory(index: number) {
    this.selectedCategory = this.categories[index];
  }

  selectProgram(category: Category, index: number) {
    if (category) {
      this.selectedProgram = category.programs[index];
    }
  }

  get CategoryCount(): number {
    return this.categories.length;
  }

  get ProgramCount(): number {
    return this.programCount;
  }

  get Categories(): Category[] {
    return this.categories;
  }

  get SelectedCategory(): Category {
    return this.selectedCategory;
  }

  get SelectedProgram(): Program {
    return this.selectedProgram;
  }

  get HasSelectedCategory(): boolean {
    return this.selectedCategory != null;
  }

  get HasSelectedProgram(): boolean {
    return this.selectedProgram != null;
  }

  private getProgramCount(): number {
    let count: number = 0;
    for (let i = 0; i < this.categories.length; i++) {
      count += this.categories[i].programs.length;
    }
    return count;
  }

}
