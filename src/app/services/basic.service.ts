import { Injectable } from '@angular/core';

export enum MessageType {
  Input,
  Output,   // normal output
  Warning,  // warning output
  Error,    // error output
}

enum InputType{
  Disabled,
  WaitForString,
  WaitForNumeric,
}

declare var Parser: any;
declare var Interpreter: any;

export interface Message {
  text: string;
  type: MessageType;
}

@Injectable({
  providedIn: 'root'
})
export class BasicService {

  private TAG_BR: string = '<BR>';
  private READY_TEXT: string = 'Ready.<br><span class="flashing-cursor"></span>';

  private interpreter: any;
  private inputType: InputType = InputType.Disabled;
  private inputCount: number = 0;
  private messages: Array<Message> = [];

  private currentProgramCode: string;
  private isRunning: boolean;

  constructor() { }

  open(programCode: string) {
    this.clear();
    try {
      this.currentProgramCode = programCode;
      this.disableInput();
      let parser: any = new Parser(programCode);
      parser.parse();
      this.interpreter = new Interpreter();
      this.interpreter.setParser(parser);

      this.interpreter.printFunction = (text: string, eol: boolean) => {
        //console.log('Print: ' + text);
        this.output(text);
        if (eol) {
          this.output(this.TAG_BR);
        }
        this.disableInput();
      };

      this.interpreter.stringInputFunction = (prompt: string, count: number) => {
        //console.log('Waiting for string input...');
        this.inputType = InputType.WaitForString;
        this.inputCount = count;
      };

      this.interpreter.numberInputFunction = (prompt: string, count: number) => {
        //console.log('Waiting for numeric input...');
        this.inputType = InputType.WaitForNumeric;
        this.inputCount = count;
      };

      this.interpreter.clsfunction = () => {
        //console.log('Clearing screen');
        this.clear();
      };

      this.interpreter.endFunction = () => {
        //console.log('Program ends');
        this.output(this.READY_TEXT, true);
        this.disableInput();
        this.isRunning = false;
      };

      this.isRunning = true;  // order is important here
      this.interpreter.interpret();
    } catch (error) {
      this.error(error, true);
    }
  }

  close() {
    this.currentProgramCode = '';
    this.interpreter = null;
    this.isRunning = false;
    this.disableInput();
  }

  clear() {
    this.messages = [];
    this.disableInput();
  }

  stop() {
    if (this.interpreter) {
      this.interpreter.forceEnd();
      this.isRunning = false;
      this.disableInput();
    }
  }

  restart() {
    if (this.currentProgramCode) {
      this.interpreter = null;
      this.isRunning = false;
      this.disableInput();
      this.open(this.currentProgramCode);
    }
  }

  input(messageText: string, forceNew?: boolean) {
    if (!this.interpreter || !this.isRunning) {
      return;
    }

    // Input handled here
    if (this.inputType != InputType.Disabled) {
      // Input message always pushed
      messageText = this.uppercase(messageText).trim();
      //console.log('Input received: ' + messageText);
      if (messageText) {
        this.pushMessage(` <span class="user-input">${messageText}</span>`, MessageType.Output);
      }
      this.pushMessage(messageText == '' ? '[RETURN]' : messageText, MessageType.Input, forceNew);

      // Text
      switch (this.inputType) {
        case InputType.WaitForString:

          if (this.inputCount > 1) {
            let values: string[] = messageText.split(",");
            if (values.length > this.inputCount) {
              this.warning("Invalid input: too many arguments", true);
              return false;
            } else {
              this.interpreter.pushInput(values);
            }
          } else {
            this.interpreter.pushInput(messageText);
          }

          try {
            this.interpreter.resumeInput();
          } catch (error) {
            this.error(error, true);
          }
          break;

        // Numeric
        case InputType.WaitForNumeric:
          // Converting text to number type

          let values: string[] = messageText.split(",");
          let numValues: number[] = [];
          if (values.length > this.inputCount) {
            this.warning("Invalid input: too many arguments", true);
            return false;
          }
          for (let i = 0; i < values.length; i++) {
            let nval = parseFloat(values[i]);
            if (isNaN(nval)) {
              this.warning("Invalid input: numeric input expected", true);
              return false;
            } else {
              numValues.push(nval);
            }
          }
          this.interpreter.pushInput(numValues);

          try {
            this.interpreter.resumeInput();
          } catch (error) {
            this.error(error, true);
          }
          break;
      }
    }
  }

  output(messageText: string, forceNew?: boolean) {
    this.pushMessage(this.uppercase(messageText), MessageType.Output, forceNew);
  }

  warning(messageText: string, forceNew?: boolean) {
    this.pushMessage(this.uppercase(messageText), MessageType.Warning, forceNew);
  }

  error(messageText: string, forceNew?: boolean) {
    this.pushMessage(this.uppercase(messageText), MessageType.Error, forceNew);
  }

  get CanReceiveInput(): boolean {
    return this.isRunning && this.inputType != InputType.Disabled;
  }

  get Messages(): Array<Message> {
    return this.messages;
  }

  get CurrentProgramCode(): string {
    return this.currentProgramCode;
  }

  get IsRunning(): boolean {
    return this.isRunning;
  }

  private getLastMessage(): Message {
    let count: number = this.messages.length;
    return count > 0 ? this.messages[count - 1] : null;
  }

  private pushMessage(messageText: string, messageType: MessageType, forceNew?: boolean) {
    // No empty text allowed
    if (!messageText) {
      return;
    }

    let lastMessage: Message = this.getLastMessage();
    // Same type? Append to the previous text instead of pushing a new message
    if (!forceNew && lastMessage && messageType == lastMessage.type) {
      let newText: string = lastMessage.text + messageText;
      // Starts with line break? Remove it.
      if (newText.indexOf(this.TAG_BR) == 0) {
        newText = newText.replace(this.TAG_BR, '');  // replaces only the first instance of value
      }
      if (newText) {
        this.messages[this.messages.length - 1].text = newText;
      }
    // Pusing new message
    } else {
      let message: Message = {
        text: messageText,
        type: messageType
      };
      this.messages.push(message);
    }
  }

  private uppercase(text: string) {
    return text && (typeof text === 'string') ? text.toUpperCase() : '';
  }

  private disableInput() {
    this.inputType = InputType.Disabled;
    this.inputCount = 0;
  }
}
