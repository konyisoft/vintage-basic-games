import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor() { }

  roundToDecimals(n: number, d: number): string {
    return n.toFixed(d);
  }
}
