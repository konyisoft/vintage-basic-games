import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

import { UtilsService } from './utils.service'

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  DEFAULT_FONT_SIZE: number = 12;
  DEFAULT_LETTER_SPACING: number = -0.6;

  fontSize: number;
  letterSpacing: number;

  private isPopoverVisible: boolean;

  constructor(
    private storage: Storage,
    private utils: UtilsService
  ) {
    this.load();
  }

  load() {
    this.reset();  // default values

    this.storage.ready().then(() => {
      this.storage.get('fontSize').then((value) => {
        if (value != null) {
          this.fontSize = value;
        }
      });
      this.storage.get('letterSpacing').then((value) => {
        if (value != null) {
          this.letterSpacing = value;
        }
      });
    });
  }

  save() {
    this.storage.ready().then(() => {
      this.storage.set('fontSize', this.fontSize);
      this.storage.set('letterSpacing', this.letterSpacing);
    });
  }

  reset() {
    this.fontSize = this.DEFAULT_FONT_SIZE;
    this.letterSpacing = this.DEFAULT_LETTER_SPACING;
  }

  setPopoverVisibility(visible: boolean) {
    this.isPopoverVisible = visible;
  }

  get FontSizeStyle(): string {
    return this.utils.roundToDecimals(this.fontSize, 0) + "";
  }

  get LetterSpacingStyle(): string {
    return this.utils.roundToDecimals(this.letterSpacing, 1) + "";
  }

  get LineHeightStyle(): string {
    // Always fontSize + 2, cannot be set by user
    return this.utils.roundToDecimals(this.fontSize + 2, 0) + "";
  }

  get IsPopoverVisible(): boolean {
    return this.isPopoverVisible;
  }
}
