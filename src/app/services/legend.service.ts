import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LegendService {

  private isPopoverVisible: boolean;

  constructor() { }

  setPopoverVisibility(visible: boolean) {
    this.isPopoverVisible = visible;
  }

  get IsPopoverVisible(): boolean {
    return this.isPopoverVisible;
  }

}
