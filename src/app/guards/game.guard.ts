import { Injectable } from '@angular/core';
import { CanDeactivate } from '@angular/router';
import { Observable, of } from 'rxjs';

import { BasicService } from '../services/basic.service';
import { GamePage } from '../pages/game/game.page';

@Injectable()
export class GameGuard implements CanDeactivate<GamePage> {

  constructor(
    private basicService: BasicService
  ) { }

  canDeactivate(gamePage: GamePage): Observable<boolean> {
    this.basicService.close();
    return of(true);
  }

}