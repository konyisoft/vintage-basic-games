# VINTAGE BASIC GAMES


![Screenshot 01](screenshots/01.png) ![Screenshot 02](screenshots/02.png) ![Screenshot 03](screenshots/03.png) ![Screenshot 04](screenshots/04.png)

## About

Play classic BASIC computer games from the 1960s/70s on your mobile device. Vintage BASIC games is a collection of early programs, currently containing **99** games in **14** categories. You can find here:

- Strategy and resource management games: Hamurabi, King, Civil War...
- Sport simulations: Bowling, Football, Hockey, Golf...
- Card games: Acey Ducey, Black Jack, Poker...
- Casino games: Craps, Horserace, Roulette...
- Logical and puzzle games: Awari, Mastermind, TicTacToe...
- Space simulations: LEM, Lunar, Star Trek...
- Adventure games,
- Educational games,
- Number and letter guessing games,
- ...and many other interesting things.

All the programs are text-based with some simple character graphics as they played 40-50 years before. The player communicates with the program in chat-style, using the keyboard. It may be answering questions, entering some numeric parameters or selecting from various options.

The application uses the open source **SWBASIC2** interpreter to execute BASIC programs: [Check it out here.](https://konyisoft.gitlab.io/swbasic/)

## Install

To try out this project, first you will have [Node.js](https://nodejs.org/en/) and [Git](https://git-scm.com/) installed on your computer.

To install Ionic and Cordova, open a terminal window and run this command:

```npm install -g ionic cordova```

Download (clone) the files of the project to your local computer:

```git clone git@gitlab.com:konyisoft/vintage-basic-games.git```

Navigate to the */vintage-basic-games* directory and run the following command to install the packages required by the project:

```npm install```

Finally, open the project in your default browser with this command:

```ionic serve --lab```

## Application

The Android build of the project can be found on Google Play: [Vintage BASIC games for Android.](https://play.google.com/store/apps/details?id=com.konyisoft.vintage_basic_games)

## License

GNU GPLv3

----

Krisztian Konya, Konyisoft, 2019  
[konyisoft.eu](https://konyisoft.eu/)
